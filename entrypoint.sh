# entrypoint.sh es un script que permite ejecutar nuestra aplicación dentro de docker
#!/bin/sh
set -e 

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'